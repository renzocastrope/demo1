
// Comentario en una línea
/*
	Bloque de comentario
*/

/*
	Tipos de Datos
	===============
	String => Texto
	Number => Números
	Object => Objeto
	Array  => Arreglos
	Function => Función
*/

var nombre = "Renzo";
var apellidos = "Castro Jurado";
var edad = 28;
var info = nombre + ' ' + apellidos;

console.log( info );
console.log( edad + '' );
console.log( String(edad) );

var i;
var resultado;
// for( valor inicia; condición; incremento )
for(i=1; i<=12; i++)
{
	resultado = i * 3;
	console.log( '3 x ' + i + ' = ' + resultado );
}

var pais = 'Perú';
console.log( pais.length );
// var persona = {};
var persona = new Object();
persona.nombre = 'Renzo';
persona.edad = 28;
persona.dni = '80808080';

console.log( persona );
console.dir( persona );


var persona2 = {
	nombre: "Renzo",
	edad: 28,
	dni: '80808080'	// Esto es dni
};
console.log( persona2 );

var persona3_tipo = {
	cargo: 'administrador',
	email: 'admin@empresa.com'
};
var persona3 = {
	nombre: "Omar",
	tipo: persona3_tipo
};

var persona4 = {
	nombre: "Omar",
	tipo: {
		cargo: 'administrador',
		email: 'admin@empresa.com'
	}
};
console.dir( persona4 );


/* ARRAYS */

//var alumnos = [];
var alumnos = new Array();
alumnos.push( "Renzo" );
alumnos.push( 28 );
alumnos.push( {email:"contact@area51.pe"} );
alumnos.push( ['Hugo','Paco','Luis'] );
console.log(alumnos);

//var alumnos2 = ['Hugo','Paco','Luis'];
var alumnos2 = [
	'Hugo',
	'Paco',
	'Luis'
];
console.log(alumnos2);

var multiplicar = [];
var j;
for(j=1; j<=12; ++j)
{
	multiplicar.push( '3 x ' + j + ' = ' + (j*3) );
}
console.dir( multiplicar );
console.log( multiplicar[3] );

var alumnos10 = [
	{
		nombre: 'Hugo',
		edad: 12
	},
	{
		nombre: 'Paco',
		edad: 11
	},
	{
		nombre: 'Luis',
		edad: 10
	}
];
console.log( alumnos10[1].edad ); // 11
console.log( alumnos10[2].nombre ); // Luis

console.log('=====================');

var mi_nombre = "Renzo Castro Jurado";
console.log( mi_nombre.charAt(0) );
console.log( mi_nombre.charAt(3) );
console.log( mi_nombre.charAt(6) );

var nombre_array = [
	'Renzo',
	'Castro',
	'Jurado'
];
console.dir(nombre_array);


if( 1 != 3 ){
	console.log('uno es distinto de 3');
}
if( 1 == 1 ){
	console.log('uno es igual a uno');
}
if( 1 < 2 ){
	console.log('uno es menor que 2');
}
if( 1 > 0 ){
	console.log('uno es mayor que cero');
}

if( "Renzo" == "Carlos" ){
	console.log('Si son iguales');
} else {
	console.log('no son iguales');
}

var texto = '';
texto += 'a';
texto += 'b';
texto += 'c';
console.log( texto );

var mi_nombre = "Renzo Castro Jurado";
var lista = [];
var h;
for(h=0; h < mi_nombre.length; h++)
{
	console.log('11111111111111');
	if( mi_nombre.charAt(h) == ' ' )
	{
		console.log('PONNIES!!');
	}
}


console.log( lista );








